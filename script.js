/*console.log('hello world');*/

// 3. Create a "trainer" object by using object literals.
// 4. Initialize/add the "trainer" object properties.
let trainer = {
	age: 10,
	friends: {hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty']},
	name: 'Ash Ketchum',
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	talk: function(){
		console.log("Result of talk method: ");
		console.log("Pikachu" + "!" + " I choose you!");
	}
}

console.log(trainer);

// 5. Access the "trainer" object properties using dot and square bracket notation.
console.log("Result from dot notation: ");
console.log(trainer.name);
console.log("Result from square bracket notation: ");
console.log(trainer['pokemon']);
// 6. Invoke/call the "trainer" object method.
trainer.talk();
// 7. Create a constructor function for creating a pokemon.


function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		target.health -= this.attack
		console.log(target.name + "'s health is now reduced to " + target.health);
	};
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
};

// 8. Create/instantiate several pokemon using the "constructor" function.
let piplup = new Pokemon("Piplup", 33);
let pikachu = new Pokemon("Pikachu", 15);
let sentret = new Pokemon("Sentret", 12);
let morpeko = new Pokemon("Morpeko", 19);

console.log(piplup);
console.log(pikachu);
console.log(sentret);
console.log(morpeko);
// 9. Have the pokemon objects interact with each other by calling on the "tackle" method.
piplup.tackle(pikachu);
pikachu.faint();